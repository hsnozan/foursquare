//
//  ViewController.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 13.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView

class HomeViewController: UIViewController {

    static let BASE_URL: String = "https://api.foursquare.com/v2/venues/search?client_id=IUYS05B222U4IL0DT1XT5D4ZDAKEC5ZRRHBYIWM1DBOWB3VO&client_secret=AGQVA5HZUCI2LH33KYO4QN312BNTEMIWIYBGVICPXHGURLMT&v=20191405"
    
    @IBOutlet weak var venueType: UITextField!
    @IBOutlet weak var locationField: UITextField!
    var venueTypeLabel: String = ""
    var locationFieldLabel: String = ""
    var locations: String!
    var results: NSArray?
    var venueModelArray = [VenueModel]()
    var venueModel: VenueModel!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocationManager()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        venueType.becomeFirstResponder()
    }
    
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: CGRect.zero, type: .lineSpinFadeLoader, color: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1), padding: 20)
        view.addSubview(loading)
        loading.translatesAutoresizingMaskIntoConstraints = false
        loading.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loading.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        return loading
    }()
    
    func setLocationManager() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    @IBAction func getVenueValue(_ sender: Any) {
        venueTypeLabel = venueType.text ?? ""
    }
    
    @IBAction func getLocationValue(_ sender: Any) {
        locationFieldLabel = locationField.text ?? ""
    }
    
    @IBAction func searchButtonClick(_ sender: Any) {
        getVenueValue(self)
        getLocationValue(self)
        let isFromCharacterset = lookIsFromCharacterset()
        if isFromCharacterset {
            if venueTypeLabel.count < 3 {
                showPopup()
            } else if locationFieldLabel.count == 0 {
                requestNearMe()
            } else {
                self.locations = "&near=" + locationFieldLabel
                requestNearMe()
            }
        } else {
            showCharacterPopup()
        }
    }
    
    func requestNearMe() {
        let fullUrl = HomeViewController.BASE_URL + self.locations + "&radius=500" + "&query=" + venueTypeLabel
        let encoded = fullUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        let url = URL(string: encoded ?? "")
        self.venueModelArray = []
        loading.startAnimating()
        URLSession.shared.dataTask(with: url!) { (data, response, error) -> Void in
            if error == nil && data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: Any]
                    let response = json["response"] as? NSDictionary
                    self.results = response?.value(forKey: "venues") as? NSArray
                    for model in self.results! {
                        let venueDictModel = model as! NSDictionary
                        let venue = VenueModel(venueModel: venueDictModel)
                        self.venueModelArray.append(venue)
                    }
                    
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "PlacesVC", sender: self)
                    }
                } catch {
                    print("JSON could not parse")
                }
            }
            }.resume()
    }
    
    func showPopup() {
        let alert = UIAlertController(title: "Bilgilendirme", message: "Lütfen mekan tipini 3 karakterden fazla giriniz.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showCharacterPopup() {
        let alert = UIAlertController(title: "Bilgilendirme", message: "Lütfen mekan tipine alfabetik karakterler giriniz.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func lookIsFromCharacterset() -> Bool {
        if venueTypeLabel.rangeOfCharacter(from: NSCharacterSet.letters) != nil {
            return true
        } else {
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        loading.stopAnimating()
        if segue.identifier == "PlacesVC" {
                if let placesVC = segue.destination as? PlacesViewController {
                    placesVC.venueModelArray = self.venueModelArray
                }
        }
    }
}

extension HomeViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.locations = "&ll=" + "\(locValue.latitude)" + "," + "\(locValue.longitude)"
    }
    
}

