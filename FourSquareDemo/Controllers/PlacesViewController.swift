//
//  PlacesViewController.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 14.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PlacesViewController: UIViewController {

    static let urlClientID = "?client_id=IUYS05B222U4IL0DT1XT5D4ZDAKEC5ZRRHBYIWM1DBOWB3VO&client_secret=AGQVA5HZUCI2LH33KYO4QN312BNTEMIWIYBGVICPXHGURLMT&v=20191405"
    @IBOutlet weak var placesTableView: UITableView!
    var venueModelArray = [VenueModel]()
    var venueDetailModel: VenueDetailModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    lazy var loading: NVActivityIndicatorView = {
        let loading = NVActivityIndicatorView(frame: CGRect.zero, type: .lineSpinFadeLoader, color: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1), padding: 20)
        view.addSubview(loading)
        loading.translatesAutoresizingMaskIntoConstraints = false
        loading.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loading.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        return loading
    }()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        loading.stopAnimating()
        if segue.identifier == "toVenueDetailVC" {
            if let indexPath = placesTableView.indexPathForSelectedRow {
                if let venueDetailVC = segue.destination as? VenueDetailViewController {
                    let dictionaryModel = venueModelArray[indexPath.row]
                    venueDetailVC.venueNameFromCell = dictionaryModel.venueNameModel
                    venueDetailVC.venueLat = dictionaryModel.venueLat
                    venueDetailVC.venueLng = dictionaryModel.venueLon
                    venueDetailVC.venueImageUrl = venueDetailModel.imageURL
                    venueDetailVC.venueTipArray = venueDetailModel.tipTextArray
                }
            }
        }
    }
    
    func reguestVenueDetail(cellModel: VenueModel) {
        let url = URL(string: "https://api.foursquare.com/v2/venues/" + cellModel.venueId + PlacesViewController.urlClientID)
        loading.startAnimating()
        URLSession.shared.dataTask(with: url!) { (data, response, error) -> Void in
            if error == nil && data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: Any]
                    let response = json["response"] as? NSDictionary
                    let venueDictModel = response?.value(forKey: "venue") as! NSDictionary
                    self.venueDetailModel = VenueDetailModel(venueDetailModel: venueDictModel)

                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "toVenueDetailVC", sender: self)
                    }
                } catch {
                    print("JSON could not parse")
                }
            }
            }.resume()
    }

}

extension PlacesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venueModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceTableViewCell") as! PlaceTableViewCell
        let cellModel = venueModelArray[indexPath.row]
        cell.venueName.text = cellModel.venueNameModel
        cell.venueAddress.text = cellModel.venueAddress
        cell.venueCity.text = cellModel.venueCity
        cell.venueCountry.text = cellModel.venueCountry
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellModel = venueModelArray[indexPath.row]
        reguestVenueDetail(cellModel: cellModel)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
