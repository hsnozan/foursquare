//
//  VenueDetailViewController.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 15.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class VenueDetailViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var venueDetailContainer: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var venueImageView: UIImageView!
    @IBOutlet weak var venueName: UILabel!
    @IBOutlet weak var venueTips: UILabel!
    var venueNameFromCell: String?
    var venueLat: Double?
    var venueLng: Double?
    var venueImageUrl: String?
    var venueTipArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateView()
    }
    func updateView() {
        let imageURl = URL(string: venueImageUrl ?? "")
        venueName.text = venueNameFromCell
        venueImageView.kf.setImage(with: imageURl)
        let annotation = MKPointAnnotation()
        let center = CLLocationCoordinate2D(latitude: venueLat ?? 0.0, longitude: venueLng ?? 0.0)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.001, longitudeDelta: 0.001))
        annotation.coordinate = center
        self.mapView.setRegion(region, animated: true)
        self.mapView.addAnnotation(annotation)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != venueDetailContainer {
            dismiss(animated: true, completion: nil)
        }
    }

}

extension VenueDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venueTipArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TipsTableViewCell") as? TipsTableViewCell
        let cellModel = venueTipArray[indexPath.row]
        cell?.tipLabel.text = cellModel
        
        return cell ?? UITableViewCell()
    }
    
    
}
