//
//  TipsTableViewCell.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 15.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class TipsTableViewCell: UITableViewCell {

    @IBOutlet weak var tipLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
