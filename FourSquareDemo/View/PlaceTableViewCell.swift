//
//  PlaceTableViewCell.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 14.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var venueName: UILabel!
    @IBOutlet weak var venueAddress: UILabel!
    @IBOutlet weak var venueCity: UILabel!
    @IBOutlet weak var venueCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
