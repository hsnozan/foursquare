//
//  File.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 14.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct VenueModel {
    
    var venueNameModel: String!
    var venueCategoriesModel: NSArray!
    var venueLocationModel: NSDictionary!
    var venueAddress: String!
    var venueCity: String!
    var venueCountry: String!
    var venueImageUrl: String!
    var venueTips: String!
    var venuePostalCode: String!
    var venueLat: Double?
    var venueLon: Double?
    var venueId: String!
    
    init(venueModel: NSDictionary) {
        self.venueNameModel = venueModel.value(forKey: "name") as? String
        self.venueId = venueModel.value(forKey: "id") as? String
        
        self.venueCategoriesModel = venueModel.value(forKey: "categories") as? NSArray
        for item in venueCategoriesModel {
            let itemModel = item as? NSDictionary
            let iconModel = itemModel?.value(forKey: "icon") as? NSDictionary
            let prefix = iconModel?.value(forKey: "prefix") as! String
            let suffix = iconModel?.value(forKey: "suffix") as! String
            self.venueImageUrl = prefix + "300x500" + suffix
        }
        
        self.venueLocationModel = venueModel.value(forKey: "location") as? NSDictionary
        self.venueAddress = venueLocationModel.value(forKey: "address") as? String
        self.venueLat = venueLocationModel.value(forKey: "lat") as? Double
        self.venueLon = venueLocationModel.value(forKey: "lng") as? Double
        self.venueCity = venueLocationModel.value(forKey: "city") as? String
        self.venueCountry = venueLocationModel.value(forKey: "country") as? String
        self.venuePostalCode = venueLocationModel.value(forKey: "postalCode") as? String
    }
    
}
