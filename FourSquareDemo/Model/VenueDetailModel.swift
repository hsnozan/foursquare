//
//  VenueDetailModel.swift
//  FourSquareDemo
//
//  Created by Ozan Al on 15.05.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation
struct VenueDetailModel {
    
    var bestPhotosModel: NSDictionary?
    var photoPrefix: String?
    var photoSuffix: String?
    var imageURL: String?
    var tipsModel: NSDictionary?
    var tipsGroupArray: NSArray?
    var tipsGroupItemsArray: NSArray?
    var tipText: String?
    var tipTextArray = [String]()
    
    init(venueDetailModel: NSDictionary) {
        self.bestPhotosModel = venueDetailModel.value(forKey: "bestPhoto") as? NSDictionary
        self.photoPrefix = bestPhotosModel?.value(forKey: "prefix") as? String
        self.photoSuffix = bestPhotosModel?.value(forKey: "suffix") as? String
        if let photoPreffix = self.photoPrefix, let photoSufix = self.photoSuffix {
            self.imageURL = photoPreffix + "original" + photoSufix
        }
        
        self.tipsModel = venueDetailModel.value(forKey: "tips") as? NSDictionary
        self.tipsGroupArray = tipsModel?.value(forKey: "groups") as? NSArray
        
        for group in tipsGroupArray ?? [] {
            let groupModel = group as? NSDictionary
            self.tipsGroupItemsArray = groupModel?.value(forKey: "items") as? NSArray
            for item in tipsGroupItemsArray ?? [] {
                let itemModel = item as? NSDictionary
                self.tipText = itemModel?.value(forKey: "text") as? String
                tipTextArray.append(tipText ?? "There is no tip for this venue")
            }
        }
    }
    
}
